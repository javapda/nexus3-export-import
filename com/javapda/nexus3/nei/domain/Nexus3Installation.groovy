package com.javapda.nexus3.nei.domain
import groovy.json.JsonSlurper
import groovy.transform.ToString


@ToString(includeNames=true, includeFields=true)
class Nexus3Installation {
    String name, nexus3Url
    String credentialsText
    Credentials credentials
    boolean credentialsEncrypted

    public String getName() {
        name
    }
    public void setName(String name) {
        this.name = name
    }

    public String getCredentialsText() {
        credentialsText
    }
    public void setCredentialsText(String credentialsText) {
        this.credentials = credentialsText?new Credentials(credentialsText):null
    }
    public Credentials getCredentials() {
        this.credentials
    }
    public void setCredentials(Credentials credentials) {
        this.credentials = credentials
    }
}
