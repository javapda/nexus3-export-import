package com.javapda.nexus3.nei.domain
import groovy.transform.ToString
@ToString(includeNames=true, includeFields=true)
class MavenAssetConfigTest extends GroovyTestCase {
    String groupId, artifactId, baseVersion, extension, classifier

    void testAsQueryString() {
        MavenAssetConfig mac = new MavenAssetConfig(groupId:'com.gs',artifactId:'gs-util',baseVersion:'8.0.9.RELEASE',extension:'jar',classifier:null)
        assert mac
        assert mac.asQueryString()
        assert "maven.groupId=com.gs&maven.artifactId=gs-util&maven.baseVersion=8.0.9.RELEASE&maven.extension=jar" == mac.asQueryString()
    }

}