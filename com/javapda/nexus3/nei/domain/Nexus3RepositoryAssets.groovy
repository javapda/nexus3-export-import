package com.javapda.nexus3.nei.domain
import groovy.json.JsonSlurper

import groovy.transform.ToString
@ToString(includeNames=true, includeFields=true)
class Nexus3RepositoryAssets {
    List<Nexus3RepositoryAsset> items
    String continuationToken

    def addItem(Nexus3RepositoryAsset item) {
        this.items.add(item)
    }

    static Nexus3RepositoryAssets fromJson(String json) {
        Map<String, Object> map = new JsonSlurper().parseText(json)
        List<Nexus3RepositoryAsset> items = map.items.collect {it->Nexus3RepositoryAsset.fromMap(it)}
        new Nexus3RepositoryAssets(
            continuationToken:map.continuationToken,
            items:items
            )
    }


}