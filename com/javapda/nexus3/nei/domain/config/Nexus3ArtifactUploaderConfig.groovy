package com.javapda.nexus3.nei.domain.config
import com.javapda.nexus3.nei.domain.*
import groovy.transform.ToString
import com.javapda.nexus3.util.*

@ToString(includeNames=true, includeFields=true)
class Nexus3ArtifactUploaderConfig {
    String repoUrl,repoName,filename
    Credentials credentials
    GAV gav
    Boolean verbose
    Nexus3ArtifactUploaderConfig verify() {
        def reasons=[]
        if (!gav) {reasons << "missing gav"}
        if (!repoUrl) {reasons << "missing repoUrl"}
        if (!repoUrlWebSiteIsLive()) {"repo website down '${repoUrl}'"}
        if (!repoName) {reasons << "missing repoName"}
        if (!filename) {reasons << "missing filename"}
        File file = new File(filename)
            if (filename && !file.exists()) {reasons << "file '${file.absolutePath}' does not exist"}
            if (file.exists()) {
            if (filename && !file.file) {reasons << "file '${file.absolutePath}' is not a file"}
            if (filename && !file.file) {reasons << "file '${file.absolutePath}' is not a file"}
            if (filename && !file.canRead()) {reasons << "unable to read file '${file.absolutePath}'"}
        }
        if (!reasons.empty) {
            throw new RuntimeException("bad upload config: ${reasons}")
        }
        this
    }

    boolean repoUrlWebSiteIsLive() {
        new WebSiteLiveVerifier(repoUrl).verify()
    }
}