package com.javapda.nexus3.nei.domain.config
import com.javapda.nexus3.nei.domain.*
import groovy.transform.ToString
import com.javapda.nexus3.util.*

@ToString(includeNames=true, includeFields=true)
class Nexus3ArtifactDownloaderConfig {
    String repoUrl,repoName,filename
    Credentials credentials
    GAV gav
    Boolean verbose
    Nexus3ArtifactDownloaderConfig verify() {
        def reasons=[]
        if (!gav) {reasons << "missing gav"}
        if (!repoUrl) {reasons << "missing repoUrl"}
        if (!repoUrlWebSiteIsLive()) {"repo website down '${repoUrl}'"}
        if (!reasons.empty) {
            throw new RuntimeException("bad download config: ${reasons}")
        }
        this
    }

    boolean repoUrlWebSiteIsLive() {
        new WebSiteLiveVerifier(repoUrl).verify()
    }
}