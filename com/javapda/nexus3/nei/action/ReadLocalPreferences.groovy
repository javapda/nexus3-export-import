package com.javapda.nexus3.nei.action
import com.javapda.nexus3.nei.domain.*
import groovy.transform.ToString

/**
*  retrieves the preferences from well-known location
*/
@Deprecated
@ToString(includeNames=true, includeFields=true)
class ReadLocalPreferences {

    static File DEFAULT_PREFERENCES_FILE=new File(System.properties['user.home'],".nexus-export-import-prefs")
    static String[] get() {
        if (DEFAULT_PREFERENCES_FILE.exists()) {
            Properties properties = new Properties()
            DEFAULT_PREFERENCES_FILE.withInputStream {
                properties.load(it)
            }
            def res = []
            properties.each{ it ->
                res+=it.key
                if (it.value) {
                    res+=it.value
                }

            }
            return res

        }
        return []
    }

    static main(args) {
        println("THIS IS @Deprecated")
    }
}