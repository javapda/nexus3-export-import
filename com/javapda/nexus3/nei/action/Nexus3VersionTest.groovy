package com.javapda.nexus3.nei.action
@Grapes(
    @Grab(group='org.junit.jupiter', module='junit-jupiter-api', version='5.6.2', scope='test')
)

import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.rest.*
import com.javapda.nexus3.urlproviders.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.BeforeEach

class Nexus3VersionTest { 
    Nexus3ExportImportPreferences preferences
    Nexus3Installation localInstallation

    @BeforeEach
    void setup() {
        assert preferences()
        assert localInstallation()
    }

    @Test
    void performVersionInfo() {
        def verionInfo = new Nexus3Version().getVersionInfo(localInstallation.nexus3Url, localInstallation.credentials)
        assert verionInfo
        assert "3.22.0-02" == verionInfo.version
        assert "Nexus/3.22.0-02 (OSS)" == verionInfo.versionText
    }

    @Test
    void performVersion() {
        def version = new Nexus3Version().getVersion(localInstallation.nexus3Url, localInstallation.credentials)
        assert version
        assert "3.22.0-02" == version
    }

    Nexus3ExportImportPreferences preferences() {
        if (!preferences) {
            preferences = Nexus3ExportImportPreferences.loadLocal()
            assert preferences
        }
        preferences
    }

    Nexus3Installation localInstallation() {
        if (!localInstallation) {
            localInstallation = preferences().getInstallation("localhost-fake")
        }
        localInstallation
    }



}