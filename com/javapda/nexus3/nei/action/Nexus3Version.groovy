package com.javapda.nexus3.nei.action
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.rest.*
import com.javapda.nexus3.urlproviders.*
import groovy.transform.ToString



class Nexus3Version {
    def getVersion(String nexus3Url, Credentials credentials) {
        // build url with maven search info
        getVersionInfo(nexus3Url,credentials).version
    }
    def getVersionInfo(String nexus3Url, Credentials credentials) {
        // build url with maven search info
        def res = new Nexus3Status().getStatus(nexus3Url,credentials)
        res.empty?"version unavailable":new Info(res[0])
    }

    @ToString(ignoreNulls=false,includeNames=true, includeFields=true )
    static class Info {
        String versionText, version
        Info(String versionText) {
            this.versionText =versionText
            parse()
        }

        // Nexus/3.22.0-02 (OSS)
        static def PATTERN_NEXUSVERSION = /^Nexus\/(\S+) \(OSS\)$/
        def parse() {
            def matches
            if ((matches = versionText =~ PATTERN_NEXUSVERSION)) {
                showGavAndMatches(versionText,matches)
                version=matches.group(1)
            } else {
                return null
            }
            this
        }

        def showGavAndMatches(versionText,matches) {
            if (false) {
                println("matches.groupCount(): ${matches.groupCount()}")
                println("pattern: ${PATTERN_NEXUSVERSION}")
                println("versionText ${versionText}")
                for (int i=0; i<= matches.groupCount();i++) {
                    println("${i} ${matches.group(i)}")
                }
            }

        }        
    }
}