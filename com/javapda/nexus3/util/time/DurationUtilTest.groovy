package com.javapda.nexus3.util.time

class DurationUtilTest extends GroovyTestCase {

    void testDuration() {
        def startTime = new Date()
        def multiplier = 1
        Thread.sleep(multiplier*1000)
        def endTime = new Date()
        def delta=(long)((endTime.getTime()-startTime.getTime())/1000)
        assert delta
        def duration = new DurationUtil().duration(startTime,endTime)
        assert duration == "00:00:0${multiplier}"
        // println("duration: ${duration}")
    }

}