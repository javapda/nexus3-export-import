package com.javapda.nexus3.testdata
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.nei.action.*
class TestData {
    Nexus3ExportImportPreferences preferences

    Nexus3Installation localInstallation,nexusInstallation

    Credentials localCredentialsFake() {
        new Credentials("fakeusername:fakepassword")
    }
    Credentials localCredentialsGsadmin() {
        new LocalCredentialsProvider().get("gsadmin")
    }
    Nexus3Config localNexus3ConfigReleases() {
        new Nexus3Config(nexus3Url:localInstallation().nexus3Url,repoName:"releases",credentials:localInstallation().credentials,verbose:true).verify()
    }
    Nexus3Config nexusNexus3ConfigReleases() {
        new Nexus3Config(nexus3Url:nexusInstallation().nexus3Url,repoName:"releases",credentials:nexusInstallation().credentials,verbose:true).verify()
    }
    Nexus3Config nexusNexus3ConfigThirdparty() {
        new Nexus3Config(nexus3Url:localInstallation().nexus3Url,repoName:"thirdparty",credentials:localInstallation().credentials,verbose:true).verify()
    }
    MavenAssetConfig mavenAssetConfigThirdpartyBfopdf() {
        new MavenAssetConfig(groupId:'bigfaceless',artifactId:'bfopdf',baseVersion:'2.20.1',extension:'jar',classifier:null)
    }
    Nexus3SearchAssetsConfig nexus3SearchAssetsConfigThirdpartyBfopdf() {
        new Nexus3SearchAssetsConfig(mavenAssetConfig:mavenAssetConfigThirdpartyBfopdf())
    }

    Nexus3ExportImportPreferences preferences() {
        if (!preferences) {
            preferences = Nexus3ExportImportPreferences.loadLocal()
            assert preferences
        }
        preferences
    }

    Nexus3Installation localInstallation() {
        if (!localInstallation) {
            localInstallation = Nexus3ExportImportPreferences.loadLocal().getInstallation("localhost-fake")
        }
        localInstallation
    }
    Nexus3Installation nexusInstallation() {
        if (!nexusInstallation) {
            nexusInstallation = Nexus3ExportImportPreferences.loadLocal().getInstallation("nexus-fake")
        }
        nexusInstallation
    }
}


/*

<dependency>
  <groupId>bigfaceless</groupId>
  <artifactId>bfopdf</artifactId>
  <version>2.20.1</version>
</dependency>

*/