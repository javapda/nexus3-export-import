
package com.javapda.nexus3
@Grapes(
    @Grab(group='org.junit.jupiter', module='junit-jupiter-api', version='5.6.2', scope='test')
)

import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.rest.*
import com.javapda.nexus3.urlproviders.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.BeforeEach

import com.javapda.nexus3.nei.domain.*

class Nexus3ApiTest {
    Nexus3ExportImportPreferences preferences
    Nexus3Installation localInstallation
    @BeforeEach
    void setup() {
        assert preferences()
        assert localInstallation()
    }

    // static listRepositories()

    @Test
    void test() {
        assert new Nexus3Api()
        assert new Nexus3Api().getVersion(localInstallation.nexus3Url, localInstallation.credentials)
    }

    Nexus3ExportImportPreferences preferences() {
        if (!preferences) {
            preferences = Nexus3ExportImportPreferences.loadLocal()
            assert preferences
        }
        preferences
    }

    Nexus3Installation localInstallation() {
        if (!localInstallation) {
            localInstallation = preferences().getInstallation("localhost-fake")
        }
        localInstallation
    }

}