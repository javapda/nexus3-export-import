package com.javapda.nexus3.rest
import groovy.transform.ToString
import groovy.json.JsonSlurper
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.urlproviders.*
import com.javapda.nexus3.nei.action.*

@ToString(includeNames=true, includeFields=true, includePackage=false)
class BinaryGet {
    File get(String url, File file, Credentials credentials=null) {
        if (file.exists()) {
            file.delete()
        }
        def connection = new URL( url )
                .openConnection() as HttpURLConnection

        if (credentials.basicAuth) {
            connection.setRequestProperty ("Authorization", credentials.basicAuth);
        }

        // set some headers
        connection.setRequestProperty( 'User-Agent', 'nexus-repo-3-groovy-2.4.4' )
        connection.setRequestProperty( 'Accept', 'application/octet-stream' )
        // get the response code - automatically sends the request
        def input = connection.getInputStream();
        // System.exit(3)
        byte[] buffer = new byte[4096];
        int n;
        OutputStream output = new FileOutputStream( file );
        while ((n = input.read(buffer)) != -1) {
            output.write(buffer, 0, n);
        }
        output.close();
        return file
    }
}