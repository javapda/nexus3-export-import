package com.javapda.nexus3.rest

class QueryStringBuilderTest extends GroovyTestCase {
    Map params

    void testBase() {
        assert new QueryStringBuilder(params:[age:43,name:'jed clampett'])
        assert new QueryStringBuilder(params:[age:43,name:'jed clampett']).get()
        assert "age=43&name=jed clampett" == new QueryStringBuilder(params:[age:43,name:'jed clampett']).get()
    }
    

}