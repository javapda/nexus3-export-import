package test
import static java.lang.String.format
import groovy.json.JsonSlurper
import groovy.transform.ToString

@ToString(includeNames=true,ignoreNulls = false)
public class SomeClass {
    String myname = "freddy"
    Integer age
    List<String> likes = ["javascript","java","groovy"]

    String format(Integer i) { 
        i.toString()
    }

    static void main(String[] args) {
        assert format('String') == 'String' 
        assert new SomeClass().format(Integer.valueOf(1)) == '1'
    }
}