# secure-maven

## run a maven command
```
mvn --settings ./secure-settings.xml validate -X
```

## deploying with maven command
```
mvn --settings ./secure-settings.xml deploy -X
```


### output
* with [maven debug flag (`-X`)](https://books.sonatype.com/mvnref-book/reference/running-sect-options.html) on the output includes evidence of authentication
```
...
username=trustedclient, password=***
...
```
* NOTE: if the wrong credentials were placed in the [`secure-settings.xml`](secure-settings.xml) a failure would occur, including `Not authorized`:
```
Caused by: org.apache.maven.wagon.authorization.AuthorizationException: Not authorized
```
* more output here:
```
[DEBUG] Using connector BasicRepositoryConnector with priority 0.0 for http://localhost:8089/repository/trustedclient-repo/ with username=trustedclient, password=***
Downloading from central: http://localhost:8089/repository/trustedclient-repo/org/apache/maven/maven-parent/33/maven-parent-33.pom
Downloaded from central: http://localhost:8089/repository/trustedclient-repo/org/apache/maven/maven-parent/33/maven-parent-33.pom (44 kB at 3.2 MB/s)
[DEBUG] Writing tracking file /tmp/nexus3-export-import-repo/org/apache/maven/maven-parent/33/_remote.repositories
[DEBUG] Writing tracking file /tmp/nexus3-export-import-repo/org/apache/maven/maven-parent/33/maven-parent-33.pom.lastUpdated
[DEBUG] Using mirror central (http://localhost:8089/repository/trustedclient-repo/) for apache.snapshots (https://repository.apache.org/snapshots).
[DEBUG] Using transporter WagonTransporter with priority -1.0 for http://localhost:8089/repository/trustedclient-repo/

```

## repository trustedclient-repo
* configured as a group repository
* contains:
  * one
  * two
* initially completely empty, but when artifacts are requested from central (e.g. by adding a new dependency `org.apache.commons:commons-lang3:3.9::jar` in the [`pom.xml`](pom.xml))

## project build plan
* notice that we write to local repository located at `/tmp/nexus3-export-import-repo`; this is because that is what we set in the [`secure-settings.xml`](secure-settings.xml).
* also, notice that all of the artifacts are tunneled through the trustedclient-repo
* NOTE: when setting up new repository I have adopted the practice of creating a nexus 3 blob for each new repository (e.g. trustedclient-blob for the trustedclient-repo).
```
[DEBUG] Writing tracking file /tmp/nexus3-export-import-repo/org/apache/maven/plugins/maven-jar-plugin/2.4/_remote.repositories
[DEBUG] Writing tracking file /tmp/nexus3-export-import-repo/org/apache/maven/plugins/maven-jar-plugin/2.4/maven-jar-plugin-2.4.jar.lastUpdated

[DEBUG] === PROJECT BUILD PLAN ================================================
[DEBUG] Project:       com.javapda:nexus-export-import:0.0.1-SNAPSHOT
[DEBUG] Dependencies (collect): []
[DEBUG] Dependencies (resolve): []
[DEBUG] Repositories (dependencies): [central (http://localhost:8089/repository/trustedclient-repo/, default, releases)]
[DEBUG] Repositories (plugins)     : [central (http://localhost:8089/repository/trustedclient-repo/, default, releases)]
[DEBUG] =======================================================================

```

## simple [pom.xml](pom.xml)
```

```

## [secure-settings.xml](secure-settings.xml)
* one big mirror to rule them all - pointing to the maven group repository (`trustedclient-repo`)
* local repository located at `/tmp/nexus3-export-import-repo`
```
<?xml version="1.0" encoding="UTF-8"?>

<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                      http://maven.apache.org/xsd/settings-1.0.0.xsd">

  <mirrors>
    <mirror>
      <id>central</id>
      <name>central</name>
      <url>http://localhost:8089/repository/trustedclient-repo/</url>
      <mirrorOf>*</mirrorOf>
    </mirror>
  </mirrors>

  <servers>
    <server>
      <id>central</id>
      <username>trustedclient</username>
      <password>trustedclient123</password>
    </server>
  </servers>

    <localRepository>/tmp/nexus3-export-import-repo</localRepository>
    <offline>false</offline>
</settings>
```

# configuring repositories
## trustedclient-repo
![repositories view showing trustedclient-repo](../images/repositories-trustedclient-repo.png)
* trustedclient-repo configuration
  * shows using Blob store of `trustedclient-blob`
  * indicates the member repositories included (maven-snaphots, maven-central, maven-releases, maven-public, thirdparty)
![repositories view showing trustedclient-repo](../images/repositories-trustedclient-repo-configuration.png)
## releases
* configuration
  * shows using Blob store of `releases-blob`
![repositories view showing trustedclient-repo](../images/repositories-releases-setup.png)
## snapshots
* configuration
  * shows using Blob store of `snapshots-blob`
![repositories view showing trustedclient-repo](../images/repositories-snapshots-setup.png)

# configuring blob stores
## trustedclient-blob
![blob view showing trustedclient-blob](../images/blob-store-trustedclient-blob.png)
* trustedclient-blob configuration
  * shows it is being used by 1 repository (i.e. `trustedclient-repo`)
![blob view showing trustedclient-blob configuration](../images/blob-store-trustedclient-blob-configuration.png)
## releases-blob
* view
![blob view showing trustedclient-blob](../images/blob-store-releases.png)
* setup
![blob view showing trustedclient-blob](../images/blob-store-releases-setup.png)
* configuration
![blob view showing trustedclient-blob](../images/blob-store-releases-configuration.png)

## back story
the whole point of this project was to enable secure (authenticated) access to a company installed [sonatype nexus 3 repository manager](https://www.sonatype.com/nexus-repository-oss).

We had been working with a nexus 2 installation allowing anonymous access. We wanted to promote to nexus 3 so we could do more with things like docker.

The migration path from nexus 2 to nexus 3 meant promoting to the latest version of nexus 2. Installing a fresh installation of nexus 3. The pulling the artifacts from the nexus 2 repository into our new nexus 3 installation.




![Error on Anonymous Access screen in Nexus 3](../images/error-on-anonymous-access.png "Error on Anonymous Access screen in Nexus 3"  )